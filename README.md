# SwissBorg Tech Challenge

🙏 Welcome to this repo SwissBorg team 🎉

This is **John Flionis**, honestly pleased to meet you, and thank you for hopping on the solution of this exciting challenge with me!

This repo attempts to provide the IaC for the AWS and k8s resources towards solving the tasks of SwissBorg's Junior SRE challenge, link to pdf [here](https://drive.google.com/file/d/10VztIaKLdRFe0zvzZqH5_zzdehfU8z5_/view?usp=sharing).

## Create a Kubernetes Cluster

### Purpose & functionality

The type of Kubernetes cluster decided to provision is an Elastic Kubrnetes Cluster (EKS) one, with the deployment on AWS being end-to-end automated via Terraform code. Kubernetes as the orchestration platform, enables diverse workload deployment and management, simplifying communication, scalability and high-availability of services.

### Configurations & justification of decisions

EKS was selevcted as the type of Kubernetes cluster to be provisioned for a multitude of reasons, briefly outlined below:
  * **Abstraction of complexity**, as the control plane of k8s is setup and managed by AWS
  * **Scalability** and **high-availability**, as the managed part comes with built-in autoscaling and multi-AZ delpoyments
  * **Seemless integration with AWS ecosystem**, as integrating with other AWS services, like IAM, ELBs, etc, is painfree
  * **Lower maintenance overhead**, as the updates of the control plane are managed and performaed automatically
  * **Easier to meet security compliance**, thanks to the integrations with other services and managed components by the cloud provider.

In order for the EKS cluster to be created a networking infrastructure needs to be already in-place. The IaC has been built in a modular way, hence, as both the [swissborg](terraform/environments/swissborg) and [eks-only](terraform/environments/eks-only) environments defines it, we make use of the following modules, which are defined under the [terraform/modules](terraform/modules) directory:
  * [eks](terraform/modules/eks)
    * a pre-existing TF module is used, i.e. [terraform-aws-modules/eks/aws](https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/latest)
      * in order to reduce development & maintenance overhead
    * an EKS managed node group is used in combination with [bottlerocket](https://aws.amazon.com/bottlerocket/) AMIs, in order to reap the benefts of:
      * higher uptime with lower operational costs
      * improved security as of automatic OS updates
      * shorter boot time due to smaller resource footprint
      * *Sidenote: [bottlerocket FAQ](https://aws.amazon.com/bottlerocket/faqs/)*
    * security best practices are followed as
      * secret encryption is enabled, using a KMS key
      * all types of control plane logging is enabled (conditionally)
      * IRSA is enabled and aws-auth ConfigMap by us, in order to enable configurability 
      * EBS volume encryption is enabled
      * only the private subnets of the VPC are passed, as all k8s services should be exposed to the outside world over ingresses and LBs
    * EBS CSI driver is installed
      * along side the required IRSA
        * via the [IAM Role for Service Accounts in EKS](https://registry.terraform.io/modules/terraform-aws-modules/iam/aws/latest/submodules/iam-role-for-service-accounts-eks) TF submodule
      * a dedicated storage class is created for `gp3` type of EBS volumes
        * via the [kubernetes_storage_class_v1](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/storage_class_v1) TF resource
  * [networking](terraform/modules/networking)
    * again, a pre-existing TF module is used, i.e. [terraform-aws-modules/vpc/aws](https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/latest)
      * in order to reduce development & maintenance overhead
    * VPC is spreading across 3 AWS AZs, in order to provide redundancy, ensure high-availability and scalability, thanks to the additional DCs
    * additionally, the module provisions VPC endpoints for S3 and ECR, in order to route traffic internally, instead of over the internet, both for latency reduction, as well as cost-management reasons

### Time & Difficulty estimation
  * Time: 4h
  * Difficulty: Easy

### How-to guide

  * Let's quickly clone and cd into the repo:

    ```bash
    git clone https://gitlab.com/devops-johnfli/assignments/swissborg.git
    cd swissborg
    ```
  
  * Then, creating the cluster should be as easy as, changning our working directory to the [swissborg](terraform/environments/swissborg) environment directory of the Terraform code:
    
    ```bash
    cd terraform/environments/swissborg
    ```

  * The typical terraform commands should suffice to create the EKS cluster:

    ```bash
    terraform init
    terraform plan
    terraform apply
    ```

  * Before doing that however, you will need to provide the values to the following variables of the module, via the [terraform/environments/swissborg/variables.tf](terraform/environments/swissborg/variables.tf?plain=1#L20-L31) file, where each variable is well-documented and pretty much self-explanatory:
    * `eks_map_admin_username`
    * `eks_map_rolename`
  * Terraform will complain for the rest of the required variables,
    * but you can, add some dummy data or comment out the rest of them:
      * `grafana_keycloak_oauth_client_secret`
      * `retool_license_key`
      * `retool_keycloak_oauth_client_secret`
    * along side with the non-infrastructure TF files, of which the whole content should be commented out as well, i.e. the following files:
      * [terraform/environments/swissborg/applications.tf](terraform/environments/swissborg/applications.tf)
      * [terraform/environments/swissborg/argocd.tf](terraform/environments/swissborg/argocd.tf)
      * [terraform/environments/swissborg/database.tf](terraform/environments/swissborg/database.tf)
      * [terraform/environments/swissborg/ingress-controller.tf](terraform/environments/swissborg/ingress-controller.tf)
      * [terraform/environments/swissborg/keycloak.tf](terraform/environments/swissborg/keycloak.tf)
      * [terraform/environments/swissborg/monitoring.tf](terraform/environments/swissborg/monitoring.tf)
   *  That being said, mentioning again here that the easier way to deploy only the EKS cluster is via the [eks-only](terraform/environments/eks-only) environment, following the same first four steps, simply changing directory into the [eks-only](terraform/environments/eks-only) directory instead of the [swissborg](terraform/environments/swissborg) one.

<details><summary><b>Resources</b></summary>

1. https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/latest
2. https://github.com/terraform-aws-modules/terraform-aws-eks/blob/v19.21.0/examples/complete/main.tf
3. https://github.com/terraform-aws-modules/terraform-aws-eks/blob/v19.21.0/examples/eks_managed_node_group/main.tf
4. https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/latest
5. https://developer.hashicorp.com/terraform/language/functions/cidrsubnet
6. https://aws.amazon.com/bottlerocket/
7. https://aws.amazon.com/bottlerocket/faqs/
8. https://docs.aws.amazon.com/eks/latest/userguide/eks-add-ons.html
9. https://docs.aws.amazon.com/eks/latest/userguide/csi-iam-role.html
10. https://docs.aws.amazon.com/eks/latest/userguide/managing-ebs-csi.html
11. https://stackoverflow.com/questions/74648632/how-do-i-use-the-aws-ebs-csi-driver-addon-when-using-the-aws-eks-terraform-modul
12. https://registry.terraform.io/modules/terraform-aws-modules/iam/aws/latest/submodules/iam-role-for-service-accounts-eks
13. https://github.com/terraform-aws-modules/terraform-aws-iam/tree/v5.33.0/modules/iam-role-for-service-accounts-eks
14. https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/storage_class_v1
15. https://kubernetes.io/docs/concepts/storage/storage-classes/
</details>

## Deploy ArgoCD (Optional)

### Purpose & functionality

ArgoCD is a popular continuous delivery tool used to deploy and keep in sync services running on kubernetes based on the manifests included in a target repo. This is pure GitOps, i.e. Operations based on Git, whatever the state of the git repo is, in terms of resource manifests, the same should be reflected, in terms of services running, i.e. resources manifests applied on the target kubernetes cluster.

### Configurations & justification of decisions

ArgoCD is deployed using the [argocd](terraform/modules/argocd/) terraform module, which under the hood uses the official [ArgoCD HELM chart](https://github.com/argoproj/argo-helm/tree/main/charts/argo-cd).

Configuration of ArgoCD is performed by consulting the official [ArgoCD docs](https://argo-cd.readthedocs.io/en/stable/), and the `values.yaml` file that is issued when installing the ArgoCD HELM chart can be found in the following repo file, [terraform/modules/eks/helm_charts/argocd/values.yaml](terraform/modules/eks/helm_charts/argocd/values.yaml), where decisions that are not self-explanatory should be followed by a comment.

Inside the [argocd](terraform/modules/argocd/) terraform module all key variables are explained and can be easily configured upon one's needs, in the [variables.tf](terraform/modules/argocd/variables.tf) file.

The [argocd](terraform/modules/argocd/) module except for installing the official [ArgoCD HELM chart](https://github.com/argoproj/argo-helm/tree/main/charts/argo-cd), leverages the [argocd-apps HELM chart](https://artifacthub.io/packages/helm/argo/argocd-apps), which would normally enable the automatic deployment of all ArgoCD apps, with zero manual effort. However, as explained in the [Failed ArgoCD Implementation for deploying HELM Charts](###FailedArgoCDImplementationfordeployingHELMCharts) paragraph below, the deployment of HELM charts via ArgoCD does not currently work as expected due to a bug, as per this [GitHub open issue](https://github.com/argoproj/argo-cd/issues/15463). Hence, we decided to simply deploy the [metrics-server](https://github.com/kubernetes-sigs/metrics-server) k8s manifests instead, for demonstration purposes.

### Time & Difficulty estimation
  * Time: 1h
  * Difficulty: Easy

### Failed ArgoCD Implementation for deploying HELM Charts

The next tasks of the tech challenge are about deploying services to the kubernetes cluster, all of which come with a HELM chart. 

ArgoCD supports deployment of HELM charts and recently they added a feature that enables [Multiple Sources for an Application](https://argo-cd.readthedocs.io/en/stable/user-guide/multiple_sources/). Leveraging this feature would allow us to apply HELM charts from the official repo, while using values YAML files configured upon our needs on this very repo.

However, currently there is a bug in this implementation, as mentioned on this [GitHub open issue](https://github.com/argoproj/argo-cd/issues/15463), and it does not function properly yet.

An implementation that would leverage ArgoCD for the purposes of automating the deployment of the rest the tasks would look like the following:
  * Under the directory [helm/values](helm/values), reside all the custom values YAML files for the HELM charts that we would normally apply in a simple:
    * `helm install -f values.yaml <release> <chart>`
  * Inside the [argocd](argocd) directory we have:
    * [apps](argocd/apps/) directory, where we seperate workloads into:
      * [swissborg](argocd/apps/swissborg/) ones, which are part of the application service, like:
        * [Retool](argocd/apps/swissborg/app/retool.app.yaml), and
        * [PostgreSQL](argocd/apps/swissborg/database/postgresql.app.yaml)
      * [system](argocd/apps/system/) apps, which consist of all the necessary utilies required for the application to run, like:
        * [traefik](argocd/apps/system/ingress/traefik.app.yaml),
        * [metrics-server](argocd/apps/system/kube-system/metrics-server.app.yaml), and
        * [monitoring](argocd/apps/system/monitoring/kube-prometheus-stack.app.yaml)
    * [projects](argocd/projects/) directory, where all the ArgoCD projects are defined, to segregate the different app types:
      * [default](argocd/projects/default.yaml) project
      * [swissborg](argocd/projects/swissborg.yaml) project
      * [system](argocd/projects/system.yaml) project
    * [apps-of-apps](argocd/argocd-apps-of-apps), where ArgoCD apps that make use of the [App of Apps ArgoCD pattern](https://argo-cd.readthedocs.io/en/stable/operator-manual/cluster-bootstrapping/#app-of-apps-pattern) to recursively iterate over all subdirectories of the [apps](argocd/apps/), as well as the [projects](argocd/projects/), and sync the state into the kubernetes cluster in a true GitOps fashion

No matter the fact that this attempt was not successful, it served as a great learning case.

<details><summary><b>Resources</b></summary>

1. https://github.com/argoproj/argo-helm/blob/main/charts/argo-cd/values.yaml
2. https://argo-cd.readthedocs.io/en/stable/user-guide/helm/
3. https://argo-cd.readthedocs.io/en/stable/user-guide/multiple_sources/#helm-value-files-from-external-git-repository
4. https://github.com/argoproj/argo-cd/blob/master/docs/operator-manual/project.yaml
5. https://argo-cd.readthedocs.io/en/stable/operator-manual/project-specification/
6. https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/
7. https://argo-cd.readthedocs.io/en/stable/user-guide/application-specification/
8. https://argo-cd.readthedocs.io/en/stable/operator-manual/cluster-bootstrapping/#app-of-apps-pattern
9. https://argo-cd.readthedocs.io/en/stable/operator-manual/ingress/#traefik-v22
10. https://artifacthub.io/packages/helm/argo/argocd-apps

</details>

## Deploy Traefik as Ingress Controller (Optional)


### Purpose & functionality

Traefik is an open-source Edge Router that simplifies publishing services. It receives requests on behalf of your system and finds out which components are responsible for handling them. We will use Traefik as an ingress controller to expose our internal k8s services to the outside world, either to the public, as internet-facing ingresses, or to the internal private networks of our infrastructure only, as internal ingresses.

### Configurations & justification of decisions

Traefik is deployed using the [traefik](terraform/modules/traefik/) terraform module, which under the hood uses the official [Traefik HELM chart](https://github.com/traefik/traefik-helm-chart/tree/master/traefik).

Configuration of Traefik is performed by consulting the official [Traefik docs](https://doc.traefik.io/traefik/getting-started/install-traefik/#use-the-helm-chart), and the `values.yaml` file that is issued when installing the Traefik HELM chart can be found in the following repo file, [terraform/modules/traefik/helm_charts/traefik/values.yaml](terraform/modules/traefik/helm_charts/traefik/values.yaml), where decisions that are not self-explanatory should be followed by a comment. 

Inside the [traefik](terraform/modules/traefik/) terraform module all key variables are explained and can be easily configured upon one's needs, in the [variables.tf](terraform/modules/traefik/variables.tf) file.

### Time & Difficulty estimation
  * Time: 4h
  * Difficulty: Easy

<details><summary><b>Resources</b></summary>

1. https://doc.traefik.io/traefik/
2. https://github.com/traefik/traefik-helm-chart/tree/master/traefik
3. https://github.com/traefik/traefik-helm-chart/blob/master/traefik/values.yaml
4. https://doc.traefik.io/traefik/getting-started/install-traefik/
5. https://artifacthub.io/packages/helm/traefik/traefik
6. https://doc.traefik.io/traefik/getting-started/install-traefik/#use-the-helm-chart

</details>


## Deploy Keycloak

### Purpose & functionality

Keycloak is an open-source identity provider service, which we can use for access and role management across our internal ecosystem, as well as external clients.

### Configurations & justification of decisions

Keycloak is deployed using the [keycloak](terraform/modules/keycloak/) terraform module, which under the hood uses the official bitnami [Keycloak HELM chart](https://github.com/bitnami/charts/tree/main/bitnami/keycloak/#installing-the-chart).

Configuration of Keycloak for user authentication and authorization is performed by consulting the official [Keycloak docs](https://wjw465150.gitbooks.io/keycloak-documentation/content/server_admin/topics/realms/create.html), and the `values.yaml` file that is issued when installing the Keycloak HELM chart can be found in the following repo file, [terraform/modules/keycloak/helm_charts/keycloak/values.yaml](terraform/modules/keycloak/helm_charts/keycloak/values.yaml), where decisions that are not self-explanatory should be followed by a comment.

Inside the [keycloak](terraform/modules/keycloak/) terraform module all key variables are explained and can be easily configured upon one's needs, in the [variables.tf](terraform/modules/keycloak/variables.tf) file.

### Time & Difficulty estimation
  * Time: 1h
  * Difficulty: Easy

<details><summary><b>Resources</b></summary>

1. https://thalesdocs.com/idpv/integrations/idpv_integrations/keycloak/set_keyclk/index.html
2. https://github.com/bitnami/charts/tree/main/bitnami/keycloak/#installing-the-chart
3. https://wjw465150.gitbooks.io/keycloak-documentation/content/server_admin/topics/realms/create.html
4. https://wjw465150.gitbooks.io/keycloak-documentation/content/server_admin/topics/users.html
5. https://wjw465150.gitbooks.io/keycloak-documentation/content/server_admin/topics/login-settings.html

</details>


## Deploy PostgreSQL

### Purpose & functionality

PostgreSQL is one of the most popular databases, open-source and relational. Databases are used to for long-term storage and persistence of data, allowing for services to be stateless, thus containing only the business logic, without requiring to retain knowledge of the state for longer than the computation per se at each moment.

### Configurations & justification of decisions

PostgreSQL is deployed using the [database](terraform/modules/database/) terraform module, which under the hood uses the official bitnami [PostgreSQL HELM chart](https://github.com/bitnami/charts/tree/main/bitnami/postgresql#installing-the-chart).

For PostgreSQL to be configured for secure and reliable data storage, it is vital to have the following in-place:
   * use secretes for database credentials
   * use persistent volumes for data persistence
   * use encryption on EBS volumes, in order for our data at rest to be protected
   * use TLS encyprtion for encrypting data in transit
   * use resource requests and limits
   * implement backup and restore strategy
   * use logging
     * also enabling audit log options might be of interest
   * use monitoring
     * enable metrics via exporter
   * use security context to set pod permissions

Configuration of PostgreSQL is achieved via the `values.yaml` file that is issued when installing the PostgreSQL HELM chart. This file can be found in [terraform/modules/database/helm_charts/postgresql/values.yaml](terraform/modules/postgresql/helm_charts/postgresql/values.yaml), where decisions that are not self-explanatory should be followed by a comment.

Inside the [database](terraform/modules/database/) terraform module all key variables are explained and can be easily configured upon one's needs, in the [variables.tf](terraform/modules/database/variables.tf) file.

### Time & Difficulty estimation
  * Time: 2h
  * Difficulty: Easy

<details><summary><b>Resources</b></summary>

1. https://www.postgresql.org/docs/
2. https://github.com/bitnami/charts/tree/main/bitnami/postgresql
3. https://github.com/bitnami/charts/blob/main/bitnami/postgresql/values.yaml
4. https://github.com/bitnami/charts/tree/main/bitnami/postgresql-ha

</details>

## Deploy Retool

### Purpose & functionality

Retool is low-code end-to-end platform, with GUI and backend, that is really popular for building custom internal tools, such as admin panels, frontends for databases and APIs, or customer support dashboards.

### Configurations & justification of decisions

Retool is deployed using the [applications](terraform/modules/applications/) terraform module, which under the hood uses the official [Retool HELM chart](https://github.com/tryretool/retool-helm/tree/main). Following the documentation provided on the official [Retool docs](https://docs.retool.com/self-hosted/quickstarts/kubernetes/helm) comes in really handy.

For Retool to be configured as the front-end of PostgreSQL, one can follow the steps taken on this [video tutorial](https://www.youtube.com/watch?v=OuNB3_ZElFk) and troubleshoot if needed.

In order to configure Retool to integrate with Keycloak for authentication, one can follow the instructions on the official Retool docs, [Configure SSO with OIDC authentication](https://docs.retool.com/sso/quickstarts/custom/oidc), and troubleshoot if needed.

Configuration of Retool is achieved via the `values.yaml` file that is issued when installing the Retool HELM chart. This file can be found in [terraform/modules/applications/helm_charts/retool/values.yaml](terraform/modules/applications/helm_charts/retool/values.yaml), where decisions that are not self-explanatory should be followed by a comment.

Inside the [applications](terraform/modules/applications/) terraform module all key variables are explained and can be easily configured upon one's needs, in the [variables.tf](terraform/modules/applications/variables.tf) file.

### Time & Difficulty estimation
  * Time: 4h
  * Difficulty: Medium

<details><summary><b>Resources</b></summary>

1. https://docs.retool.com/self-hosted/quickstarts/kubernetes/helm
2. https://github.com/tryretool/retool-helm/tree/main
3. https://github.com/tryretool/retool-helm/blob/main/values.yaml
4. https://docs.retool.com/sso/quickstarts/custom/oidc
5. https://hub.docker.com/r/tryretool/backend/tags?page=1&ordering=last_updated
6. https://www.youtube.com/watch?v=OuNB3_ZElFk

</details>

## Deploy Monitoring

### Purpose & functionality

Prometheus and Grafana are probably the most popular and widely-adopted tools for scraping metrics and visualizing them, respectively. They are open-source and highly-scalable and truly versatile, thanks to their big and active communities. Monitoring tools enable engineers to observe the status of the services, verify uptime, spot anomalies, understand and troubleshoot bugs, by collecting, transforming, analyzing, and vizualizing data from system componenets.

### Configurations & justification of decisions

Prometheus and Grafana are deployed using the [monitoring](terraform/modules/monitoring/) terraform module, which under the hood uses the official [kube-prometheus-stack](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack) HELM chart. This HELM chart deploys the whole stack, i.e. Prometheus, Grafana, Alert Manager, kube-state-metrics, etc., along side [Prometheus Operator](https://github.com/prometheus-operator/prometheus-operator), which simplifies the deployment, configuration and maintenance of monitoring resources.

In order to collect metrics from the deployed components, in most cases we need to enable their ServiceMonitors or their dedicated exporter service (e.g. for PostgreSQL). In order for Prometheus to scrape these ServiceMonitors, they need to have an extra `release: <helm-release-name>` label.

In order to configure Grafana to integrate with Keycloak for authentication, one can follow the instructions on the official Grafana docs, [Configure Keycloak OAuth2 authentication](https://grafana.com/docs/grafana/latest/setup-grafana/configure-security/configure-authentication/keycloak/), and troubleshoot if needed.

Configuration of Prometheus, Grafana and the rest of the monitoring components is achieved via the `values.yaml` file that is issued when installing the kube-prometheus-stack HELM chart. This file can be found in [terraform/modules/monitoring/helm_charts/kube-prometheus-stack/values.yaml](terraform/modules/monitoring/helm_charts/kube-prometheus-stack/values.yaml), where decisions that are not self-explanatory should be followed by a comment.

Inside the [monitoring](terraform/modules/monitoring/) terraform module all key variables are explained and can be easily configured upon one's needs, in the [variables.tf](terraform/modules/monitoring/variables.tf) file.

### Time & Difficulty estimation
  * Time: 1h
  * Difficulty: Easy

<details><summary><b>Resources</b></summary>

1. https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack
2. https://github.com/prometheus-community/helm-charts/blob/main/charts/kube-prometheus-stack/values.yaml
3. https://medium.com/israeli-tech-radar/how-to-create-a-monitoring-stack-using-kube-prometheus-stack-part-1-eff8bf7ba9a9
4. https://github.com/prometheus-operator/kube-prometheus/issues/1392
5. https://grafana.com/docs/grafana/latest/setup-grafana/configure-security/configure-authentication/keycloak/
</details>

## Production-readiness

High-availability, security, scalability, and disaster recovery are the four essential pillars for production-readiness.

As mostly mentioned in the [PostgreSQL](##DeployPostgreSQL) section, as well, we have to take into consideration the following aspects towards meeting production-readiness:
   * **High-availability**
     * use 2, 3 or more replicas per service to ensure redundancy in case of outage, error, etc.
     * use resource requests and limits
     * use pod disruption budgets to guarantee service availability at all times
     * use rolling updates for zero-downtime during new service version deployments
     * use logging
       * also enabling audit log options might be of interest
       * alerting based on error logs
     * use monitoring
       * scapre metrics via dedicated exporter and visualize on dashboards
       * scapre metrics via service monitor and visualize on dashboards
       * alerting based on key metrics trends
     * use startup/readiness/liveness probes to start accepting traffic or restart the service if not responsive
     * use `topologySpreadConstaints` or `podAntiAffinity` to distribute the workloads of key services across the nodes and AZs, in order to have redundancy in case one node or one AZs faces problems
     * use combination of `nodeSelectors` and `tolerations` with `taints` to segregate specific workloads onto dedicated nodes if required
   * **Security**
     * use secrets for credentials and sensitive information
     * use encryption on EBS volumes, in order for our data at rest to be protected
     * use TLS encyprtion for encrypting data in transit
     * use network policies, service meshes, or eBPF solutions to control the communication between pod and services
     * use security context to set pod permissions
     * use load balancers via ingress controllers and API gateways to expose services to the internet
     * use WAF and DDoS protection
     * use logging
       * also enabling audit log options might be of interest
       * also logging analytics
   * **Scalability**
     * use autoscaling via HPA or KEDA or VPA for services, like Prometheus that are not scalable horizontally
     * use multiple AZs
     * use mutliple regions
     * use regions & AZs where bigger DCs reside
   * **Disaster recovery**
     * use persistent volumes for data persistence
     * enable `"Retain"` mode for pvc with critical data, like db ones
     * implement backup and restore strategy
     * live replication of database
     * maybe have a standby replica ready

A more consise **list** of **actions** for **production-readiness** would be the following:
   * **Use managed a database solution**
     * instead of having the maintenance overhead and complexity of managing the PostgreSQL ourselves, I would consider offloading that part to a managed service. [Amazon RDS](https://aws.amazon.com/rds/) and [Amazon Aurora RDS](https://aws.amazon.com/rds/aurora/) are nice-fitting candidates; setup and comliance will be easier as well.
   * **Use secrets for passing passwords and sensitive data, instead of passing them via HELM `values.yaml` files**
   * **Deploy internal Treafik ingresses for Retool, Keycloak and Grafana, in order to be accessible over VPN or intra-VPC**
     * except if the requirement is to for any of these to be publicly accessible
     * update all URLs in TF modules to make use of ingress hosts instead of k8s services (which was used only for quick demo/testing)
   * **Use Karpenter for cluster autoscaling**
     * Deploy [Karpenter](https://karpenter.sh/) for cluster autoscaling, to dynamically provision more nodes into the cluster in a very fast and versatile way
   * **Segregate & scale monitoring workloads**
     * Use Karpenter to add `taints` to nodes, in combination with `nodeSelectors` & `tolerations` on pods of monitoring componenets to isolate them and increase their reliability
     * Setup horizontal autoscaling for Grafana, to be able to handle more users and heavy queries
     * Setup vertical pod autoscaling for Prometheus replicas, to be able to handle more timeseries
       * Based on traffic magnitude; consider using a more scalable TSDB solution for storing timeseries, like [Mimir](https://grafana.com/docs/mimir/latest/)
   * **Use autoscaling for Keycloak**
   * **Use dedicated storage class for persistent volume provisioning**
   * **Use `podAntiAffinity` or `topologySpreadConstraints` to distribute workloads whenever possible**
   * **Deploy a logging aggregation solution**
     * since we already have Grafana deployed, a fitting & scalable solution would be [Loki](https://grafana.com/docs/loki/latest/)
   * **Enable persistence for Prometheus' data**
   * **Configure alerting upon key log & metric occurrence**
     * import and/or create custom dasboards on Grafana
     * identify critical metrics and log message patterns that may indicate the existence of problems in the infrastructure or application components
     * configure alerting upon them, in order for the operations team to get informed and take remediation actions in a timely manner

*Note: Another way to check out all of my production-readiness recommended configurations per component is searching by the term* `production-readiness` *inside the* `values.yaml` *files of each components.*