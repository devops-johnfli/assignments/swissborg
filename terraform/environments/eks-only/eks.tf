module "eks" {
  source = "../../modules/eks"

  providers = {
    aws        = aws
    kubernetes = kubernetes
  }

  aws_region = var.aws_region

  env_name = var.env_name

  eks_cluster_name                    = local.cluster_name
  eks_ebs_csi_driver_role_name        = "AmazonEKS_EBS_CSI_DriverRole"
  eks_ebs_csi_storage_class_name      = "ebs-sc"
  eks_k8s_version                     = "1.27"
  eks_managed_nodegroup_min_size      = 2
  eks_managed_nodegroup_max_size      = 5
  eks_managed_nodegroup_desired_size  = 2
  eks_managed_nodegroup_instance_type = "t3a.micro"

  eks_map_roles = [
    {
      rolearn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/${var.eks_map_rolename}"
      username = "${var.eks_map_rolename}"
      groups   = ["system:masters"]
    },
  ]
  eks_map_users = [
    {
      userarn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/${var.eks_map_admin_username}"
      username = "${var.eks_map_admin_username}"
      groups   = ["system:masters"]
    },
  ]

  vpc_id              = module.networking.vpc_id
  vpc_private_subnets = module.networking.vpc_private_subnets_ids
}

# Datasources
data "aws_caller_identity" "current" {}
