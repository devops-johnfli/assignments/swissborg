module "postgresql" {
  source     = "../../modules/database"
  depends_on = [module.eks]

  providers = {
    kubernetes = kubernetes
    kubectl    = kubectl
    helm       = helm
  }

  postgresql_namespace_name = "database"

  postgresql_chart_version = "13.2.24"

  pgsql_admin_password = "admin"

  pgsql_username = "retool"
  pgsql_password = "retool"
  pgsql_database = "retool"

  prometheus_scraping_label_value = module.monitoring.release_name

  storage_class_name = module.eks.cluster_ebs_csi_storage_class_name
}
