module "retool" {
  source     = "../../modules/applications"
  depends_on = [module.postgresql]

  providers = {
    kubernetes = kubernetes
    kubectl    = kubectl
    helm       = helm
  }

  retool_namespace_name = "application"

  retool_chart_version = "6.0.11"

  retool_license_key = var.retool_license_key
  retool_image_tag   = "3.20.11"

  pgsql_host = "postgresql.database.svc.cluster.local"
  pgsql_port = "5432"

  pgsql_retool_username = "retool"
  pgsql_retool_password = "retool"
  pgsql_retool_database = "retool"

  retool_base_domain                   = "http://localhost:3000"
  retool_keycloak_oauth_client_id      = "retool"
  retool_keycloak_oauth_client_secret  = var.retool_keycloak_oauth_client_secret
  retool_keycloak_oauth_realm          = "swissborg"
  retool_keycloak_oauth_auth_root_url  = "http://localhost:8081"
  retool_keycloak_oauth_token_root_url = "http://keycloak.identify.svc.cluster.local"

  storage_class_name = module.eks.cluster_ebs_csi_storage_class_name
}
