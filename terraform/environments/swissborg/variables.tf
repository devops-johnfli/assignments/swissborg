# AWS
variable "aws_region" {
  type        = string
  description = "The AWS region where the environment will be deployed"
  default     = "eu-central-1"
}
# ENV
variable "env_name" {
  type        = string
  description = "The name of the environment"
  default     = "swissborg"
}
variable "vpc_cidr" {
  type        = string
  description = "The CIDR block that will be used for the VPC"
  default     = "10.0.0.0/16"
}

####################################################################################################################################
# IMPORTANT: (action needed)
# VARIABLES BELOW THIS POINT HAVE TO BE SETUP ON Infrastructure as Code management tool (e.g. CLI, Terraform Cloud, Spacelift, etc.)
####################################################################################################################################

# AWS
variable "eks_map_admin_username" {
  type        = string
  description = "The username of an AWS user to explicitly assign admin access to for the EKS cluster (required to be set)"
}
variable "eks_map_rolename" {
  type        = string
  description = "The role name of an AWS role to assign admin access to for the EKS cluster (required to be set)"
}
# Monitoring
variable "grafana_keycloak_oauth_client_secret" {
  type        = string
  description = "Keycloak client secret for Grafana oauth integration (required to be set)"
}
# Retool
variable "retool_license_key" {
  type        = string
  description = "Retool license key (required to be set)"
}
variable "retool_keycloak_oauth_client_secret" {
  type        = string
  description = "Keycloak client secret for Retool oauth integration (required to be set)"
}
