module "monitoring" {
  source     = "../../modules/monitoring"
  depends_on = [module.eks]

  providers = {
    kubernetes = kubernetes
    kubectl    = kubectl
    helm       = helm
  }

  monitoring_namespace_name = "monitoring"

  kube_prometheus_stack_chart_version = "55.5.0"
  kube_prometheus_stack_releasename   = "monitoring"

  grafana_admin_password = "prom-operator"
  grafana_root_url       = "http://localhost:3000"

  grafana_keycloak_oauth_client_id                = "grafana"
  grafana_keycloak_oauth_client_secret            = var.grafana_keycloak_oauth_client_secret
  grafana_keycloak_oauth_realm                    = "swissborg"
  grafana_keycloak_oauth_auth_root_url            = "http://localhost:8081"
  grafana_keycloak_oauth_token_root_url           = "http://keycloak.identify.svc.cluster.local"
  grafana_keycloak_oauth_api_root_url             = "http://keycloak.identify.svc.cluster.local"
  grafana_keycloak_oauth_signout_root_url         = "http://localhost:8081"
  grafana_keycloak_oauth_post_logout_redirect_uri = "http%3A%2F%2Flocalhost%3A3000%2Flogin"
}
