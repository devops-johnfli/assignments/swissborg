module "traefik" {
  source     = "../../modules/traefik"
  depends_on = [module.eks]

  providers = {
    kubernetes = kubernetes
    kubectl    = kubectl
    helm       = helm
  }

  traefik_namespace_name = "ingress"

  traefik_chart_version = "26.0.0"

  prometheus_scraping_label_value = module.monitoring.release_name

  storage_class_name = module.eks.cluster_ebs_csi_storage_class_name
}
