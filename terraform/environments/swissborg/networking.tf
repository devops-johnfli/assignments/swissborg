module "networking" {
  source = "../../modules/networking"

  aws_region    = var.aws_region
  cluster_names = [local.cluster_name]
  env_name      = var.env_name
  vpc_cidr      = var.vpc_cidr
  # Disabling VPC flow logs during testing for cost management reasons
  vpc_flow_logs_bucket_arn = ""
}
