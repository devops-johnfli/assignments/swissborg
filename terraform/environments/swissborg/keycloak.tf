module "keycloak" {
  source     = "../../modules/keycloak"
  depends_on = [module.eks]

  providers = {
    kubernetes = kubernetes
    kubectl    = kubectl
    helm       = helm
  }

  keycloak_namespace_name = "identify"

  keycloak_chart_version = "17.3.6"

  keycloak_admin_username = "admin"
  keycloak_admin_password = "admin"

  prometheus_scraping_label_value = module.monitoring.release_name

  keycloak_pgsql_username = "keycloak"
  keycloak_pgsql_password = "keycloak"
  keycloak_pgsql_database = "keycloak"

  storage_class_name = module.eks.cluster_ebs_csi_storage_class_name
}
