module "argocd" {
  source     = "../../modules/argocd"
  depends_on = [module.eks]

  providers = {
    kubernetes = kubernetes
    kubectl    = kubectl
    helm       = helm
  }

  argocd_namespace_name = "argocd"

  argocd_chart_version = "5.51.6"

  prometheus_scraping_label_value = module.monitoring.release_name
}
