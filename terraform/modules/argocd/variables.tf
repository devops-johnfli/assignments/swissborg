variable "argocd_namespace_name" {
  type        = string
  description = "The namespace on which ArgoCD will be deployed"
  default     = "argocd"
}

variable "argocd_chart_version" {
  type        = string
  description = "The HELM chart version of ArgoCD that will be deployed"
  default     = "5.51.6"
}

variable "prometheus_scraping_label_value" {
  type        = string
  description = "The value of the label for scraping via Prometheus Service Monitor"
  default     = "prometheus"
}
