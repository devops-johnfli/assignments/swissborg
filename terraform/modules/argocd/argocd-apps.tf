resource "helm_release" "argocd-apps" {
  depends_on = [helm_release.argocd]
  namespace  = kubernetes_namespace.argocd.metadata.0.name
  wait       = true
  timeout    = 300

  name       = "argocd-apps"
  chart      = "argocd-apps"
  repository = "https://argoproj.github.io/argo-helm"
  values = [
    file("${path.module}/helm_charts/argocd-apps/values.yaml")
  ]
}
