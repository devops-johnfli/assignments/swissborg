resource "kubernetes_namespace" "argocd" {
  metadata {
    name = var.argocd_namespace_name
  }
}

resource "helm_release" "argocd" {
  depends_on = [kubernetes_namespace.argocd]

  namespace = kubernetes_namespace.argocd.metadata.0.name
  wait      = true
  timeout   = 300

  name       = "argocd"
  chart      = "argo-cd"
  repository = "https://argoproj.github.io/argo-helm"
  version    = var.argocd_chart_version

  values = [
    templatefile("${path.module}/helm_charts/argocd/values.yaml", {
      scraping-label-value = var.prometheus_scraping_label_value,
    })
  ]
}

# data "kubectl_path_documents" "argocd-manifests" {
#   pattern = "${path.module}/helm_charts/argocd/ingress.yaml"
#   vars = {
#     argocd_dns = "argocd.${var.dns}"
#   }
# }
# resource "kubectl_manifest" "argocd" {
#   for_each   = data.kubectl_path_documents.argocd-manifests.manifests
#   yaml_body  = each.value
# }
