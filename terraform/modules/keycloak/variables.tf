variable "keycloak_namespace_name" {
  type        = string
  description = "The namespace on which Keycloak will be deployed"
  default     = "identify"
}

variable "keycloak_chart_version" {
  type        = string
  description = "The HELM chart version of Keycloak that will be deployed"
  default     = "17.3.6"
}

variable "keycloak_admin_username" {
  type        = string
  description = "Keycloak admin username"
  default     = "admin"
}

variable "keycloak_admin_password" {
  type        = string
  description = "Keycloak admin password"
  default     = "admin"
}

variable "prometheus_scraping_label_value" {
  type        = string
  description = "The value of the label for scraping via Prometheus Service Monitor"
  default     = "prometheus"
}

variable "keycloak_pgsql_username" {
  type        = string
  description = "Keycloak PostgreSQL username"
  default     = "keycloak"
}

variable "keycloak_pgsql_password" {
  type        = string
  description = "Keycloak PostgreSQL password"
  default     = "keycloak"
}

variable "keycloak_pgsql_database" {
  type        = string
  description = "Keycloak PostgreSQL database"
  default     = "keycloak"
}

variable "storage_class_name" {
  type        = string
  description = "The name of the StorageClass object to be used for the PVs"
  default     = ""
}
