resource "kubernetes_namespace" "keycloak" {
  metadata {
    name = var.keycloak_namespace_name
  }
}

resource "helm_release" "keycloak" {
  depends_on = [kubernetes_namespace.keycloak]

  namespace = var.keycloak_namespace_name
  wait      = true
  timeout   = 300

  name       = "keycloak"
  chart      = "keycloak"
  repository = "oci://registry-1.docker.io/bitnamicharts"
  version    = var.keycloak_chart_version

  values = [
    templatefile("${path.module}/helm_charts/keycloak/values.yaml", {
      keycloak-admin-username = var.keycloak_admin_username,
      keycloak-admin-password = var.keycloak_admin_password,

      scraping-label-value = var.prometheus_scraping_label_value,

      keycloak-pgsql-username = var.keycloak_pgsql_username,
      keycloak-pgsql-password = var.keycloak_pgsql_password,
      keycloak-pgsql-database = var.keycloak_pgsql_database,

      storage-class-name = var.storage_class_name,
    })
  ]
}
