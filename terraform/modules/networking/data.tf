data "aws_availability_zones" "available" {
  exclude_names = var.excluded_azs
}

data "aws_caller_identity" "current" {}

data "aws_region" "current" {}