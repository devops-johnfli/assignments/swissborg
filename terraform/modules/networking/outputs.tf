output "vpc_azs" {
  value = module.vpc.azs
}
output "vpc_id" {
  value = module.vpc.vpc_id
}

output "vpc_name" {
  value = module.vpc.name
}
output "vpc_cidr" {
  value = module.vpc.vpc_cidr_block
}
output "vpc_private_subnets_ids" {
  value = module.vpc.private_subnets
}

output "vpc_private_subnets_cidr" {
  value = module.vpc.private_subnets_cidr_blocks
}
output "vpc_public_subnets_ids" {
  value = module.vpc.public_subnets
}

output "vpc_public_subnets_cidr" {
  value = module.vpc.public_subnets_cidr_blocks
}
output "vpc_intra_subnets_ids" {
  value = module.vpc.intra_subnets
}

output "vpc_intra_subnets_cidr" {
  value = module.vpc.intra_subnets_cidr_blocks
}
