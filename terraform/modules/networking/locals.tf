locals {
  # Grab 3 AZs to meet both redundancy requirements, as well as compatibility with subnet CIDRs split below 
  azs = slice(data.aws_availability_zones.available.names, 0, 3)

  private_subnets = [for k, v in local.azs : cidrsubnet(var.vpc_cidr, 4, k)]
  public_subnets  = [for k, v in local.azs : cidrsubnet(var.vpc_cidr, 8, k + 48)]
  intra_subnets   = [for k, v in local.azs : cidrsubnet(var.vpc_cidr, 8, k + 52)]

  tags = {
    for cluster in var.cluster_names : "kubernetes.io/cluster/${cluster}" => "shared"
  }
}