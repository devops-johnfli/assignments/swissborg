resource "aws_vpc_endpoint" "s3" {
  vpc_id          = module.vpc.vpc_id
  service_name    = "com.amazonaws.${var.aws_region}.s3"
  route_table_ids = module.vpc.private_route_table_ids
  tags = {
    Name = "s3"
  }
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
        "Action": "*",
        "Effect": "Allow",
        "Resource": "*",
        "Principal": "*"
    }
  ]
}
POLICY
}

resource "aws_security_group" "allow_tls" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description      = "TLS"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}

resource "aws_vpc_endpoint" "ecr" {
  vpc_id            = module.vpc.vpc_id
  service_name      = "com.amazonaws.${var.aws_region}.ecr.dkr"
  subnet_ids        = slice(module.vpc.private_subnets, 0, 2)
  vpc_endpoint_type = "Interface"
  tags = {
    Name = "ecr"
  }

  security_group_ids = [
    aws_security_group.allow_tls.id,
  ]

  private_dns_enabled = true
}
