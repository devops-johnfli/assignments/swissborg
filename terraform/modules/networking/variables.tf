variable "aws_region" {
  type        = string
  description = "The AWS region where the VPC will be deployed"
  default = "eu-central-1"
}

variable "cluster_names" {
  type        = list(string)
  description = "List of cluster names using that networking stack"
  default = []
}

variable "env_name" {
  type        = string
  description = "The name of the environment this VPC will be part of"
  default = "test"
}

variable "excluded_azs" {
  type        = list(string)
  description = "List of AZ names to be excluded for VPC"
  default = []
}

variable "vpc_cidr" {
  type        = string
  description = "The CIDR block of the VPC"
  default = "10.0.0.0/16"
}

variable "vpc_flow_logs_bucket_arn" {
  type        = string
  description = "ARN of the S3 bucket for VPC flow logs"
  default = ""
}
