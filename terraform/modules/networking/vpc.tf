module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 5.0"

  name = "${var.env_name}-vpc"
  cidr = var.vpc_cidr

  azs             = local.azs
  private_subnets = local.private_subnets 
  public_subnets  = local.public_subnets 
  intra_subnets   = local.intra_subnets 

  enable_nat_gateway = true
  single_nat_gateway     = false
  one_nat_gateway_per_az = true
  enable_dns_hostnames   = true

  public_subnet_tags = merge({
    "kubernetes.io/role/elb" = "1"
    "tier"                   = "public"
  }, local.tags)

  private_subnet_tags = merge({
    "kubernetes.io/role/internal-elb" = "1"
    "tier"                            = "private"
  }, local.tags)

  tags = local.tags
}

resource "aws_flow_log" "vpc_flow_logs" {
  count = var.vpc_flow_logs_bucket_arn != "" ? 1 : 0
  log_destination      = var.vpc_flow_logs_bucket_arn
  log_destination_type = "s3"
  traffic_type         = "ALL"
  vpc_id               = module.vpc.vpc_id
}
