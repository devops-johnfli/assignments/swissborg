variable "postgresql_namespace_name" {
  type        = string
  description = "The namespace on which PostgreSQL will be deployed"
  default     = "database"
}

variable "postgresql_chart_version" {
  type        = string
  description = "The HELM chart version of PostgreSQL that will be deployed"
  default     = "13.2.24"
}

variable "pgsql_admin_password" {
  type        = string
  description = "PostgreSQL admin password"
  default     = "admin"
}

variable "pgsql_username" {
  type        = string
  description = "PostgreSQL username"
  default     = "retool"
}

variable "pgsql_password" {
  type        = string
  description = "PostgreSQL password"
  default     = "retool"
}

variable "pgsql_database" {
  type        = string
  description = "PostgreSQL database"
  default     = "retool"
}

variable "prometheus_scraping_label_value" {
  type        = string
  description = "The value of the label for scraping via Prometheus Service Monitor"
  default     = "prometheus"
}

variable "storage_class_name" {
  type        = string
  description = "The name of the StorageClass object to be used for the PVs"
  default     = ""
}
