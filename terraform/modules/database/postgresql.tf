resource "kubernetes_namespace" "postgresql" {
  metadata {
    name = var.postgresql_namespace_name
  }
}

resource "helm_release" "postgresql" {
  depends_on = [kubernetes_namespace.postgresql]

  namespace = var.postgresql_namespace_name
  wait      = true
  timeout   = 300

  name       = "postgresql"
  chart      = "postgresql"
  repository = "oci://registry-1.docker.io/bitnamicharts"
  version    = var.postgresql_chart_version

  values = [
    templatefile("${path.module}/helm_charts/postgresql/values.yaml", {
      pgsql-admin-password = var.pgsql_admin_password,
      pgsql-username       = var.pgsql_username,
      pgsql-password       = var.pgsql_password,
      pgsql-database       = var.pgsql_database,

      scraping-label-value = var.prometheus_scraping_label_value,

      storage-class-name = var.storage_class_name,
    })
  ]
}
