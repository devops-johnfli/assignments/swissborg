variable "monitoring_namespace_name" {
  type        = string
  description = "The namespace on which the monitoring stack will be deployed"
  default     = "monitoring"
}

variable "kube_prometheus_stack_chart_version" {
  type        = string
  description = "The HELM chart version of Kubernetes Prometheus Stack that will be deployed"
  default     = "55.5.0"
}

variable "kube_prometheus_stack_releasename" {
  type        = string
  description = "The HELM chart release name of Kubernetes Prometheus Stack that will be deployed"
  default     = "monitoring"
}

variable "grafana_admin_password" {
  type        = string
  description = "Grafana admin password for dashboard login"
  default     = "prom-operator"
}

variable "grafana_root_url" {
  type        = string
  description = "Grafana root URL (update accordingly for ingress)"
  default     = "http://localhost:3000"
}

variable "grafana_keycloak_oauth_client_id" {
  type        = string
  description = "Keycloak client ID for Grafana oauth integration"
  default     = "grafana"
}

variable "grafana_keycloak_oauth_client_secret" {
  type        = string
  description = "Keycloak client secret for Grafana oauth integration"
}

variable "grafana_keycloak_oauth_realm" {
  type        = string
  description = "Keycloak realm for Grafana oauth integration"
  default     = "swissborg"
}

variable "grafana_keycloak_oauth_auth_root_url" {
  type        = string
  description = "Keycloak oauth auth root URL (update accordingly for ingress)"
  default     = "http://localhost:8081"
}

variable "grafana_keycloak_oauth_token_root_url" {
  type        = string
  description = "Keycloak oauth token URL (update accordingly for ingress)"
  default     = "http://keycloak.identify.svc.cluster.local"
}

variable "grafana_keycloak_oauth_api_root_url" {
  type        = string
  description = "Keycloak oauth api URL (update accordingly for ingress)"
  default     = "http://keycloak.identify.svc.cluster.local"
}

variable "grafana_keycloak_oauth_signout_root_url" {
  type        = string
  description = "Keycloak oauth signout root URL (update accordingly for ingress)"
  default     = "http://localhost:8081"
}

variable "grafana_keycloak_oauth_post_logout_redirect_uri" {
  type        = string
  description = "Grafana post logout redirect URI (update accordingly for ingress)"
  default     = "http%3A%2F%2Flocalhost%3A3000%2Flogin"
}
