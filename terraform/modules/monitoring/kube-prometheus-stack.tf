resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = var.monitoring_namespace_name
  }
}

resource "helm_release" "kube_prometheus_stack" {
  depends_on = [ kubernetes_namespace.monitoring ]

  namespace = var.monitoring_namespace_name
  wait      = true
  timeout   = 300

  name       = "monitoring"
  chart      = "kube-prometheus-stack"
  repository = "https://prometheus-community.github.io/helm-charts"
  version    = "${var.kube_prometheus_stack_chart_version}"

  values = [
    templatefile("${path.module}/helm_charts/kube-prometheus-stack/values.yaml", {
      grafana-admin-password  = var.grafana_admin_password,
      grafana-root-url        = var.grafana_root_url,

      grafana-keycloak-oauth-client-id                = var.grafana_keycloak_oauth_client_id,
      grafana-keycloak-oauth-client-secret            = var.grafana_keycloak_oauth_client_secret,
      grafana-keycloak-oauth-realm                    = var.grafana_keycloak_oauth_realm,
      grafana-keycloak-oauth-auth-root-url            = var.grafana_keycloak_oauth_auth_root_url,
      grafana-keycloak-oauth-token-root-url           = var.grafana_keycloak_oauth_token_root_url,
      grafana-keycloak-oauth-api-root-url             = var.grafana_keycloak_oauth_api_root_url,
      grafana-keycloak-oauth-signout-root-url         = var.grafana_keycloak_oauth_signout_root_url,
      grafana-keycloak-oauth-post-logout-redirect-uri = var.grafana_keycloak_oauth_post_logout_redirect_uri,
    })
  ]
}