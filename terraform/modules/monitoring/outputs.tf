output "release_name" {
  description = "The release name of the deployed kube_prometheus_stack HELM chart"
  value       = helm_release.kube_prometheus_stack.name
}
