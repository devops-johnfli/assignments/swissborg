# Source: https://docs.aws.amazon.com/eks/latest/userguide/csi-iam-role.html
# Source: https://github.com/terraform-aws-modules/terraform-aws-iam/blob/v5.33.0/examples/iam-role-for-service-accounts-eks/main.tf
module "ebs_csi_irsa_role" {
  source = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"

  role_name             = var.eks_ebs_csi_driver_role_name
  attach_ebs_csi_policy = true

  oidc_providers = {
    ex = {
      provider_arn               = module.eks.oidc_provider_arn
      namespace_service_accounts = ["kube-system:ebs-csi-controller-sa"]
    }
  }
}

# Source: https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/storage_class_v1
# Source: https://stackoverflow.com/questions/70075203/how-to-use-volume-gp3-in-storage-class-on-eks
resource "kubernetes_storage_class_v1" "ebs_sc" {
  depends_on = [module.ebs_csi_irsa_role]
  metadata {
    name = var.eks_ebs_csi_storage_class_name
  }
  storage_provisioner = "ebs.csi.aws.com"
  reclaim_policy      = "Retain"
  parameters = {
    type = "gp3"
  }
  allow_volume_expansion = true
  volume_binding_mode    = "WaitForFirstConsumer"
}
