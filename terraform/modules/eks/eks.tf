module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "~> 19.21.0"

  aws_auth_roles = var.eks_map_roles
  aws_auth_users = var.eks_map_users

  cluster_encryption_config = { "resources" : ["secrets"] }
  create_kms_key            = true

  cluster_endpoint_public_access = true
  cluster_name                   = var.eks_cluster_name
  subnet_ids                     = var.vpc_private_subnets

  cluster_version = var.eks_k8s_version

  cluster_addons = {
    aws-ebs-csi-driver = {
      most_recent                 = true
      resolve_conflicts_on_update = "PRESERVE"
      service_account_role_arn    = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/${var.eks_ebs_csi_driver_role_name}"
    }
  }

  manage_aws_auth_configmap = true

  enable_irsa = true
  # Set eks_control_plane_logging_enabled = true, for control plane logging to be turned on
  cluster_enabled_log_types = var.eks_control_plane_logging_enabled ? ["api", "audit", "authenticator", "controllerManager", "scheduler"] : []
  # Source: https://docs.aws.amazon.com/eks/latest/userguide/control-plane-logs.html

  tags = {
    "environment" = var.env_name
  }

  vpc_id = var.vpc_id

  eks_managed_node_groups = {
    # Default node group - as provided by AWS EKS using Bottlerocket
    bottlerocket_default = {
      # By default, the module creates a launch template to ensure tags are propagated to instances, etc.,
      # so we need to disable it to use the default template provided by the AWS EKS managed node group service
      use_custom_launch_template = false
      launch_template_name       = ""

      ami_type       = "BOTTLEROCKET_x86_64"
      platform       = "bottlerocket"
      min_size       = var.eks_managed_nodegroup_min_size
      max_size       = var.eks_managed_nodegroup_max_size
      desired_size   = var.eks_managed_nodegroup_desired_size
      instance_types = [var.eks_managed_nodegroup_instance_type]
      labels = {
        "environment"               = var.env_name
        "${var.env_name}/lifecycle" = "ondemand"
        "${var.env_name}/node_type" = "CPU"
      }
    }
  }

  prefix_separator                   = "-"
  iam_role_name                      = var.eks_cluster_name
  cluster_security_group_name        = var.eks_cluster_name
  cluster_security_group_description = "EKS cluster security group."
}

resource "aws_ebs_encryption_by_default" "default_ebs_encryption" {
  enabled = true
}

# Datasources
data "aws_caller_identity" "current" {}

