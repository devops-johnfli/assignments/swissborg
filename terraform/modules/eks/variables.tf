# AWS
variable "aws_region" {
  type        = string
  description = "The AWS region where the VPC will be deployed"
  default     = "eu-central-1"
}

# EKS
variable "eks_cluster_name" {
  type        = string
  description = "The name of the EKS cluster"
  default     = "test"
}
variable "eks_control_plane_logging_enabled" {
  type        = bool
  description = "Enable all types of control plane logging of the EKS cluster"
  default     = false
}
variable "eks_ebs_csi_driver_role_name" {
  type        = string
  description = "The name of the EBS CSI driver IAM role that will be created for the EKS cluster"
  default     = "AmazonEKS_EBS_CSI_DriverRole"
}
variable "eks_ebs_csi_storage_class_name" {
  type        = string
  description = "The name of the EBS CSI StorageClass that will be created for the EKS cluster"
  default     = "ebs-sc"
}
variable "eks_k8s_version" {
  type        = string
  description = "The k8s version that the EKS cluster will run on"
  default     = "1.27"
}
variable "eks_managed_nodegroup_min_size" {
  type        = number
  description = "The minimum number of nodes for the EKS-managed nodegroup"
  default     = 2
}
variable "eks_managed_nodegroup_max_size" {
  type        = number
  description = "The maximum number of nodes for the EKS-managed nodegroup"
  default     = 10
}
variable "eks_managed_nodegroup_desired_size" {
  type        = number
  description = "The desired number of nodes for the EKS-managed nodegroup"
  default     = 2
}
variable "eks_managed_nodegroup_instance_type" {
  type        = string
  description = "The EC2 instance type of nodes for the EKS-managed nodegroup"
  default     = "t3a.large"
}
variable "eks_map_roles" {
  description = "Additional IAM roles to add to the aws-auth configmap."
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))
  default = []
}
variable "eks_map_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))
  default = []
}

# ENV
variable "env_name" {
  type        = string
  description = "The name of the environment this VPC will be part of"
  default     = "test"
}

# VPC
variable "vpc_id" {
  type        = string
  description = "The VPC ID in which the EKS cluster will be deployed onto"
}
variable "vpc_private_subnets" {
  type        = list(string)
  description = "The list of private subnets IDs of the VPC in which the EKS cluster will be deployed onto"
}
