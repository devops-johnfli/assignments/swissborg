resource "kubernetes_namespace" "traefik" {
  metadata {
    name = var.traefik_namespace_name
  }
}

resource "helm_release" "traefik" {
  depends_on = [kubernetes_namespace.traefik]

  namespace = var.traefik_namespace_name
  wait      = true
  timeout   = 300

  name       = "traefik"
  chart      = "traefik"
  repository = "https://traefik.github.io/charts"
  version    = var.traefik_chart_version

  values = [
    templatefile("${path.module}/helm_charts/traefik/values.yaml", {
      scraping-label-value = var.prometheus_scraping_label_value,
      storage-class-name   = var.storage_class_name,
    })
  ]
}
