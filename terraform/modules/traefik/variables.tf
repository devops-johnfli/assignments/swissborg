variable "traefik_namespace_name" {
  type        = string
  description = "The namespace on which Traefik will be deployed"
  default     = "ingress"
}

variable "traefik_chart_version" {
  type        = string
  description = "The HELM chart version of Traefik that will be deployed"
  default     = "26.0.0"
}

variable "prometheus_scraping_label_value" {
  type        = string
  description = "The value of the label for scraping via Prometheus Service Monitor"
  default     = "prometheus"
}

variable "storage_class_name" {
  type        = string
  description = "The name of the StorageClass object to be used for the PVs"
  default     = ""
}
