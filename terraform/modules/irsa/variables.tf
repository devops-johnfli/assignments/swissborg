variable "cluster_oidc_issuer_url" {}

variable "oidc_provider_arn" {}

variable "namespace" {}

variable "policies" {}

variable "service_account" {}
