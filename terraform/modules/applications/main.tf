resource "kubernetes_namespace" "retool" {
  metadata {
    name = var.retool_namespace_name
  }
}

resource "helm_release" "retool" {
  depends_on = [kubernetes_namespace.retool]

  namespace = var.retool_namespace_name
  wait      = true
  timeout   = 300

  name       = "retool"
  chart      = "retool"
  repository = "https://charts.retool.com"
  version    = var.retool_chart_version

  values = [
    templatefile("${path.module}/helm_charts/retool/values.yaml", {
      retool-license-key = var.retool_license_key,

      retool-image-tag = var.retool_image_tag,

      pgsql-host            = var.pgsql_host,
      pgsql-port            = var.pgsql_port,
      pgsql-retool-username = var.pgsql_retool_username,
      pgsql-retool-password = var.pgsql_retool_password,
      pgsql-retool-database = var.pgsql_retool_database,

      retool-base-domain                   = var.retool_base_domain,
      retool-keycloak-oauth-client-id      = var.retool_keycloak_oauth_client_id,
      retool-keycloak-oauth-client-secret  = var.retool_keycloak_oauth_client_secret,
      retool-keycloak-oauth-realm          = var.retool_keycloak_oauth_realm,
      retool-keycloak-oauth-auth-root-url  = var.retool_keycloak_oauth_auth_root_url,
      retool-keycloak-oauth-token-root-url = var.retool_keycloak_oauth_token_root_url,

      storage-class-name = var.storage_class_name,
    })
  ]
}
