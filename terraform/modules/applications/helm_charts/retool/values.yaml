# Source: https://github.com/tryretool/retool-helm/blob/main/values.yaml
config:
  licenseKey: ${retool-license-key}
  useInsecureCookies: false

  # TODO: Research this parameter
  encryptionKey:
  # encryptionKeySecretName is the name of the secret where the encryption key is stored (can be used instead of encryptionKey)
  # encryptionKeySecretName:
  # encryptionKeySecretKey is the key in the k8s secret, default: encryption-key
  # encryptionKeySecretKey:

  # TODO: Research this parameter
  jwtSecret:
  # jwtSecretSecretName is the name of the secret where the jwt secret is stored (can be used instead of jwtSecret)
  # jwtSecretSecretName:
  # jwtSecretSecretKey is the key in the k8s secret, default: jwt-secret
  # jwtSecretSecretKey:

  postgresql:
    host: ${pgsql-host}
    port: ${pgsql-port}
    user: ${pgsql-retool-username}
    password: ${pgsql-retool-password}
    db: ${pgsql-retool-database}
    ssl_enabled: true

image:
  repository: "tryretool/backend"
  # You need to pick a specific tag here, this chart will not make a decision for you
  tag: ${retool-image-tag}
  pullPolicy: "IfNotPresent"


# TODO: Setup Keycloak oauth for Retool
# Source: https://docs.retool.com/sso/quickstarts/custom/oidc
environmentVariables:
    - name: BASE_DOMAIN
      value: ${retool-base-domain}
    - name: CUSTOM_OAUTH2_SSO_CLIENT_ID
      value: ${retool-keycloak-oauth-client-id}
    - name: CUSTOM_OAUTH2_SSO_CLIENT_SECRET
      value: ${retool-keycloak-oauth-client-secret}
    - name: CUSTOM_OAUTH2_SSO_SCOPES
      value: "openid email profile offline_access"
    - name: CUSTOM_OAUTH2_SSO_AUTH_URL
      value: ${retool-keycloak-oauth-auth-root-url}/realms/${retool-keycloak-oauth-realm}/protocol/openid-connect/auth
    - name: CUSTOM_OAUTH2_SSO_TOKEN_URL
      value: ${retool-keycloak-oauth-token-root-url}/realms/${retool-keycloak-oauth-realm}/protocol/openid-connect/token
    - name: CUSTOM_OAUTH2_SSO_JWT_EMAIL_KEY
      value: "email" # alt: "idToken.email"
    - name: CUSTOM_OAUTH2_SSO_JWT_FIRST_NAME_KEY
      value: "given_name" # alt: "idToken.given_name"
    - name: CUSTOM_OAUTH2_SSO_JWT_LAST_NAME_KEY
      value: "family_name" # alt: "idToken.family_name"
      # Source: https://stackoverflow.com/questions/74593497/keycloak-jwt-to-contain-user-real-and-client-role

    # TODO: Feed parameters via secret deployed by TF or sercret manager for production-readiness
    # - name: CUSTOM_OAUTH2_SSO_CLIENT_SECRET
    #   valueFrom:
    #     secretKeyRef:
    #       name: retool-oauth-secret
    #       key: keycloak-oauth-client-secret

# TODO: Enable ingress creation for external access
ingress:
  enabled: false
  # ingressClassName:
  labels: {}
  annotations: {}
  # kubernetes.io/ingress.class: nginx
  # kubernetes.io/tls-acme: "true"
  hosts:
  # - host: retool.example.com
  #   paths:
  #     - path: /
  tls:
  # - secretName: retool.example.com
  #   hosts:
  #     - retool.example.com
  # servicePort: service-port
  pathType: ImplementationSpecific
  # For supporting other ingress controllers that require customizing the .backend.service.name and .backend.service.port.name,
  # like AWS ALB extraPaths allows that customization, and it takes precedence in the list of paths of for the host,
  # this is in order to allow a rule like ssl-redirect from port 80-->443 to be first ( otherwise there wouldn't be a redirect )
  #   extraPaths:
  #     - path: /*
  #       backend:
  #         service:
  #           name: ssl-redirect
  #           port:
  #             name: use-annotation
  #       pathType: ImplementationSpecific

serviceAccount:
  create: true

# TODO: Setup startup/readiness/liveness probes for production readiness [DONE]
livenessProbe:
  enabled: true
  path: /api/checkHealth
  initialDelaySeconds: 30
  timeoutSeconds: 10
  failureThreshold: 3

readinessProbe:
  enabled: true
  path: /api/checkHealth
  initialDelaySeconds: 30
  timeoutSeconds: 10
  periodSeconds: 10
  successThreshold: 5

startupProbe:
  enabled: true
  path: /api/checkHealth
  initialDelaySeconds: 60
  timeoutSeconds: 10
  periodSeconds: 10
  successThreshold: 5

# TODO: Set resources for procuction readiness [DONE]
resources:
  limits:
    cpu: 4096m
    memory: 8192Mi
  requests:
    cpu: 2048m
    memory: 4096Mi

priorityClassName: ""

# TODO: Use pod anti affinity for better service reliability towards meeting production-readiness [DONE]
affinity:
  podAntiAffinity:
    preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 100
        podAffinityTerm:
          labelSelector:
            matchExpressions:
              - key: "app.kubernetes.io/name"
                operator: In
                values:
                  - retool
          topologyKey: "kubernetes.io/hostname"


# TODO: Use node selector & tolerations in combination with Karpenter towards meeting production-readiness 
nodeSelector: {}
tolerations: []

revisionHistoryLimit: 3

# TODO: Set PDB to guarantee lower downtime for Retool towards meeting production-readiness [DONE]
podDisruptionBudget:
  maxUnavailable: "50%"

# TODO: Use more than 2 replicas for redundancy towards meeting production-readiness [DONE]
replicaCount: 2

workflows:
  # Disable workflows
  enabled: false

codeExecutor:
  # Disable code executor
  enabled: false

retool-temporal-services-helm:
  # Disable to spin up a new Temporal Cluster alongside Retool
  enabled: false

persistentVolumeClaim:
  enabled: false
  accessModes:
    - ReadWriteOnce
  size: "5Gi"
  # TODO: Set storage class for production-readiness [DONE]
  storageClass: ${storage-class-name}
  mountPath: /retool_backend/pv-data

# TODO: Set security context for production readiness [DONE]
securityContext:
  enabled: true
  allowPrivilegeEscalation: false
  runAsUser: 1000
  fsGroup: 2000
