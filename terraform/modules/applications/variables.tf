variable "retool_namespace_name" {
  type        = string
  description = "The namespace on which Retool will be deployed"
  default     = "application"
}

variable "retool_chart_version" {
  type        = string
  description = "The HELM chart version of Retool that will be deployed"
  default     = "6.0.11"
}

variable "retool_license_key" {
  type        = string
  description = "Retool license key"
}

variable "retool_image_tag" {
  type        = string
  description = "Retool admin password"
  default     = "3.20.11"
  # Source: https://hub.docker.com/r/tryretool/backend/tags?page=1&ordering=last_updated
}

variable "pgsql_host" {
  type        = string
  description = "PostgreSQL host"
  default     = "postgresql.database.svc.cluster.local"
}

variable "pgsql_port" {
  type        = string
  description = "PostgreSQL port"
  default     = "5432"
}

variable "pgsql_retool_username" {
  type        = string
  description = "Retool PostgreSQL username"
  default     = "retool"
}

variable "pgsql_retool_password" {
  type        = string
  description = "Retool PostgreSQL password"
  default     = "retool"
}

variable "pgsql_retool_database" {
  type        = string
  description = "Retool PostgreSQL database"
  default     = "retool"
}

variable "retool_base_domain" {
  type        = string
  description = "Retool root URL (update accordingly for ingress)"
  default     = "http://localhost:3000"
}

variable "retool_keycloak_oauth_client_id" {
  type        = string
  description = "Keycloak client ID for Retool oauth integration"
  default     = "retool"
}

variable "retool_keycloak_oauth_client_secret" {
  type        = string
  description = "Keycloak client secret for Retool oauth integration"
}

variable "retool_keycloak_oauth_realm" {
  type        = string
  description = "Keycloak realm for Retool oauth integration"
  default     = "swissborg"
}

variable "retool_keycloak_oauth_auth_root_url" {
  type        = string
  description = "Keycloak oauth auth root URL (update accordingly for ingress)"
  default     = "http://localhost:8081"
}

variable "retool_keycloak_oauth_token_root_url" {
  type        = string
  description = "Keycloak oauth token URL (update accordingly for ingress)"
  default     = "http://keycloak.identify.svc.cluster.local"
}

variable "storage_class_name" {
  type        = string
  description = "The name of the StorageClass object to be used for the PVs"
  default     = ""
}
